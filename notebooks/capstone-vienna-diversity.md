# Diversity Distribution in Vienna #

## 1. Introduction ##

### 1.1 Background ###
I am in Vienna for 20 years now. In this time i moved 4 times and lived in totally different districts.
Every districts looks quite different. I now live in a more "divers" district with lots of other cultures
and many shops, restaurants and people on the street. I wonder if you can quantify this diversity?

### 1.2 Problem ###
What is the most diverse district in Vienna?
This Data can be relevant in many different fields. For example when you move to Vienna or
when you want to find a new restaurant or location to meet with your friends.
Data should be used to quantify this Question, the results will then be also a interpretation from my side.


## 2. Data ##

### 2.1 Data acquisition ###
For this project I will tap two different Data Sources:

**Wikipedia**:
A list of districts in Vienna can be found from different sources.
I use Wikipedia because there is one table with numbers on inhabitants and size.
Both are needed for the project.

**Foursquare**:
The Foursquare API will be used to gather Venue data.
This is the most important Data for this analysis.

### 2.2 Data normalization ###

In order to be able for comparison the data will be normalized.
For that the radius for Foursquare will be calculated by computing it via the district size.


## 3. Methodology ##

### Data Preperation ###   

First the Data from Wikipedia was downloaded and formatted. Then all the venues for each district were 
supplemented. For this, with the area of each district, a radius was distributed. In total 660 venues in 
66 unique categories were found.  

These venues were sorted after frequency for each venue (1 to 10) and all the venues were one hot encoded in order to ensure an efficient kmean algorithm.

### Kmean Modeling ###

As I would do the qunatative analysis by myself Kmean was not necessarily. But it was very interesting for me what this ML Model would analyse within a unsupervised approach. For this manner a kmean cluster with the 5th order was trainend and the cluster were analysed manually.

### Qualitative Analysis ### 

For the final result I did a qualitative analysis in order to come to a result. 

## 4. Results ##

|        | **Neighborhood**     | **Cluster Label Names**  | **1st Most Common Venue** | **2nd Most Common Venue** | **3rd Most Common Venue** | **4th Most Common Venue** | **5th Most Common Venue** |
| ------ | -------------------- | ------------------------ | ------------------------- | ------------------------- | ------------------------- | ------------------------- | ------------------------- |
| **0**  | Innere Stadt         | General Restaurants      | Restaurant                | Austrian Restaurant       | Italian Restaurant        | Café                      | Bakery                    |
| **1**  | Leopoldstadt         | General Restaurants      | Restaurant                | Café                      | Seafood Restaurant        | Italian Restaurant        | American Restaurant       |
| **2**  | Landstraße           | Divers and mixed cuisine | Pizza Place               | Italian Restaurant        | Restaurant                | Café                      | Gastropub                 |
| **3**  | Wieden               | Austrian cuisine         | Austrian Restaurant       | Italian Restaurant        | Restaurant                | Café                      | Bakery                    |
| **4**  | Margareten           | Divers and mixed cuisine | Indian Restaurant         | Pizza Place               | Italian Restaurant        | Austrian Restaurant       | Bakery                    |
| **5**  | Mariahilf            | Divers and mixed cuisine | Sushi Restaurant          | Café                      | Asian Restaurant          | Thai Restaurant           | Japanese Restaurant       |
| **6**  | Neubau               | Divers and mixed cuisine | Austrian Restaurant       | Asian Restaurant          | Mediterranean Restaurant  | Café                      | Tapas Restaurant          |
| **7**  | Josefstadt           | Divers and mixed cuisine | Italian Restaurant        | Café                      | Chinese Restaurant        | Sushi Restaurant          | Japanese Restaurant       |
| **8**  | Alsergrund           | General Restaurants      | Restaurant                | Pizza Place               | Vietnamese Restaurant     | Korean Restaurant         | Café                      |
| **9**  | Favoriten            | General Restaurants      | Restaurant                | Austrian Restaurant       | Fast Food Restaurant      | Café                      | Cigkofte Place            |
| **10** | Simmering            | General Restaurants      | Fast Food Restaurant      | Asian Restaurant          | Restaurant                | Pizza Place               | Bakery                    |
| **11** | Meidling             | Bakery and Coffee        | Fast Food Restaurant      | Café                      | Asian Restaurant          | Bakery                    | Falafel Restaurant        |
| **12** | Hietzing             | General Restaurants      | Restaurant                | Italian Restaurant        | Bakery                    | Chinese Restaurant        | Greek Restaurant          |
| **13** | Penzing              | General Restaurants      | Restaurant                | Asian Restaurant          | Italian Restaurant        | Austrian Restaurant       | Food                      |
| **14** | Rudolfsheim-Fünfhaus | Austrian cuisine         | Austrian Restaurant       | Café                      | Gastropub                 | Italian Restaurant        | Restaurant                |
| **15** | Ottakring            | Austrian cuisine         | Austrian Restaurant       | Restaurant                | Food                      | Greek Restaurant          | BBQ Joint                 |
| **16** | Hernals              | Austrian cuisine         | Austrian Restaurant       | Gastropub                 | Restaurant                | Café                      | Indian Restaurant         |
| **17** | Währing              | Bakery and Coffee        | Bakery                    | Café                      | Gastropub                 | Austrian Restaurant       | Italian Restaurant        |
| **18** | Döbling              | Austrian cuisine         | Austrian Restaurant       | Restaurant                | Italian Restaurant        | Café                      | Bakery                    |
| **19** | Brigittenau          | Bakery and Coffee        | Bakery                    | Italian Restaurant        | Gastropub                 | Café                      | Asian Restaurant          |
| **20** | Floridsdorf          | General Restaurants      | Restaurant                | Pizza Place               | Asian Restaurant          | Austrian Restaurant       | Italian Restaurant        |
| **21** | Donaustadt           | Bakery and Coffee        | Gastropub                 | Café                      | Bakery                    | Restaurant                | Fast Food Restaurant      |
| **22** | Liesing              | Asian                    | Sushi Restaurant          | Asian Restaurant          | Austrian Restaurant       | Bakery                    | Breakfast Spot            |
| **23** | Stadt Wien\\n        | Bakery and Coffee        | Café                      | Austrian Restaurant       | French Restaurant         | Japanese Restaurant       | Pizza Place               |

### Qualitative Analysis ### 

The goal of this project was to find the most divers district. The measure for diversity 
implemented is the diversity of restaurants. This concludes that districts with various restaurants
are more divers. When analysing the result table following districts are most divers: 
- Margareten (5th)
- Mariahilf (6th)
- Neubau (7th)
- Josefstadt (8th)
- Alsergrund (9th)

Josefstadt and Mariahilf have more restaurants in the Asian cuisine.   
the most divers is Nebau, altough the 1st most common venue is a austrian restaurant. 

### Kmean Clustering ###

The Kmean Clustering was just an expirement as I wanted to know if the clustering would be sufficient enough when not supervised. 
Surprisingly the categories are corresponding to the values I got from the qualitative analysis.

## Discuss Observations ## 

Looking at the results, especially of the Kmean clustering, they are surprisingly coincident to what
can be read in reports of vienna. Neubau is a hipster district with very divers inhabitant and very divers cousine. 

This is especially interesting as foursquare has not as many saved venues as hoped in the forefield. 

Another interesting finding is the category of Vienna as hole city. 
When looking at the most common venues you have Cafes and Austrian Restaurants on first and second place. 
Also the KMean Cluster represents Cafes and Bakers. Which is not really surprising as the cafe scene is what
Vienna is famous for. 

## Conclusion ##

This project was an interesting first step into data science.
It learnt be how easy it is to generate some data and make a qualitative analysis. 




